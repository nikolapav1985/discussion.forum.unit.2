#!/usr/bin/python

from f import sectorArea
import math

if __name__ == "__main__":
    r = 30
    angle = 50*(math.pi/180)
    print sectorArea(r,angle)
    print sq
    """
        sq defined as local variable inside sectorArea

        output

        392.699081699
        Traceback (most recent call last):
          File "./qc.py", line 10, in <module>
            print sq
        NameError: name 'sq' is not defined

        sq is a local variable. it is not visible outside of a function.

        file qc.py
    """
