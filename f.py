#!/usr/bin/python

def arcLength(r, theta):
    """
        Compute length of arc given circle radius and angle of rotation

        file f.py
        
        Parameters:
        r (float): radius of a circle
        theta (float): angle of rotation in radians pi rad = 180 deg
        
        Returns:
        len (float): arc length
    """
    return r*theta

def sectorArea(r, theta):
    """
        Compute sector area given circle radius and angle of rotation

        file f.py
        
        Parameters:
        r (float): radius of a circle
        theta (float): angle of rotation in radians pi rad = 180 deg
        
        Returns:
        area (float): sector area
    """
    sq = r*r
    return float(0.5)*sq*theta
