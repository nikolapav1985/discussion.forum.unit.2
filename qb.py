#!/usr/bin/python

from f import arcLength
import math

if __name__ == "__main__":
    r = 30
    angle = 50*(math.pi/180)
    print arcLength(30,0.87)
    print arcLength(r,angle)
    print arcLength(30,50*(math.pi/180))
    """
        first call using values 30 0.87
        second call using variables r angle
        third call call using expression 30 50*(math.pi/180)
        file qb.py
    """
