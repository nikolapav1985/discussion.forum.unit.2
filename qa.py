#!/usr/bin/python

from f import arcLength
import math

if __name__ == "__main__":
    r = 30
    angle = 50*(math.pi/180)
    print arcLength(r,angle)
    """
        function arguments r theta
        function parameters in this example 30 50*(math.pi/180)
        file qa.py
    """
