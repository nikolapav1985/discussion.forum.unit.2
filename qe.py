#!/usr/bin/python

from f import sectorArea
import math

if __name__ == "__main__":
    sq = 35*35
    print sectorArea(30,50*(math.pi/180))
    print sq
    """
        output

        392.699081699
        1225

        the program runs correctly. local sq value is 35*35=1225. sq defined inside of a function sectorArea is 30*30=900 and it is not affected.

        file qe.py
    """
