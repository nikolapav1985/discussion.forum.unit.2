Discussion forum unit 2
-----------------------

Examples regarding discussion unit 2.

Included files
--------------

f.py (functions declared here), qa.py (example call a function), qb.py (three different examples call a function), qc.py (example try using local variable), qd.py (example try using function parameter), qe.py (example variable name same as local variable).

Test environment
----------------

OS lubuntu 16.04 lts kernel version 4.13.0. Python version 2.7.12.

Example test
------------

Type ./qa.py (or any other file).
